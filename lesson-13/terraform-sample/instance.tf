# Deploy EC2 instance using Terraform

resource "aws_security_group" "my_security_group" {
  name        = "tf-public-sg"
  description = "Allow SSH from MyIP"
  vpc_id      = "vpc-0fe049cb9a9224e12" # Default VPC ID

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["90.156.105.168/32"] # OZ IP adresimiz
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["90.156.105.168/32"] # OZ IP adresimiz
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "tf-public-sg"
  }
}

resource "aws_instance" "my_ec2_instance" {
  ami                    = "ami-09d3b3274b6c5d4aa" # AMI ID deyishe biler, ishlemese yeni ID yazin.
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.my_security_group.id]
  key_name               = "ec2-key-pem" # Evvelceden yaratdigimiz EC2 Key

  user_data = <<EOF
    #!/bin/bash
    yum install -y httpd
    echo "<h1>Deployed via Terraform</h1>" > /var/www/html/index.html
    systemctl enable httpd
    systemctl start httpd
  EOF

  tags = {
    Name      = "tf-ec2-instance"
    CreatedBy = "Terraform"
  }
}

output "instance_id" {
  description = "EC2 Instance ID"
  value       = aws_instance.my_ec2_instance.id
}

output "instance_public_ip" {
  description = "EC2 Instance Public IP"
  value       = aws_instance.my_ec2_instance.public_ip
}

