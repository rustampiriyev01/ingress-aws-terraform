# VPC
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc

resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr_block
  instance_tenancy     = "default"
  enable_dns_support   = true  # DNS resolution within VPC
  enable_dns_hostnames = true # Public DNS hostnames

  tags = {
    Name = "${var.prefix}-${var.vpc_name}"
  }
}

# PUBLIC SUBNET
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet

resource "aws_subnet" "public_subnet" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.public_subnet_cidr
  availability_zone       = var.public_subnet_az
  map_public_ip_on_launch = true

  tags = {
    Name = "${var.prefix}-${var.public_subnet_name}-${var.public_subnet_az}"
  }

  depends_on = [aws_internet_gateway.igw]
}

# PRIVATE SUBNET

resource "aws_subnet" "private_subnet" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.private_subnet_cidr
  availability_zone       = var.private_subnet_az
  map_public_ip_on_launch = false

  tags = {
    Name = "${var.prefix}-${var.private_subnet_name}-${var.private_subnet_az}"
  }
}

# INTERNET GATEWAY
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/internet_gateway

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = join("-", [var.prefix, var.igw_name])
  }
}

# ELASTIC IP FOR NAT GATEWAY
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eip

resource "aws_eip" "natgw_eip" {
  vpc        = true
  depends_on = [aws_internet_gateway.igw]

  tags = {
    Name = join("-", [var.prefix, var.natgw_eip_name])
  }
}

# NAT GATEWAY
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/nat_gateway

resource "aws_nat_gateway" "natgw" {
  allocation_id = aws_eip.natgw_eip.id
  subnet_id     = aws_subnet.public_subnet.id

  tags = {
    Name = join("-", [var.prefix, var.natgw_name])
  }

  depends_on = [aws_internet_gateway.igw]
}


