# VARIABLES

variable "lesson" {
  description = "Lesson number"
}

variable "prefix" {
  type        = string
  description = "Prefix for the resource names and Name tags"
}

variable "region" {
  default     = "us-east-1"
  description = "AWS Region"
}

variable "my_ip" {
  description = "My Public IP address"
}

variable "vpc_name" {}
variable "vpc_cidr_block" {}

variable "public_subnet_name" {}
variable "public_subnet_az" {}
variable "public_subnet_cidr" {}

variable "private_subnet_name" {}
variable "private_subnet_az" {}
variable "private_subnet_cidr" {}

variable "igw_name" {}

variable "natgw_name" {}
variable "natgw_eip_name" {}

variable "public_rtb_name" {}
variable "private_rtb_name" {}

variable "public_sg_name" {}
variable "private_sg_name" {}

variable "ec2_key_name" {}

variable "public_server_name" {}
variable "private_server_name" {}



