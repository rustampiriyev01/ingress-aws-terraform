### Deploy EC2 instance using Terraform

# Get the latest Amazon Linux AMI

data "aws_ami" "amazon_linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel*-hvm-*"]
  }
}

# Define a local variable

locals {
  project = "ingress-project"
}

# Create a security group

resource "aws_security_group" "webapp_security_group" {
  name        = var.security_group_name
  description = "Allow traffic from MyIP"
  vpc_id      = var.vpc_id

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.my_ip]
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [var.my_ip]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "webapp-sg"
  }

  lifecycle {
    # Necessary if changing 'name' or 'name_prefix' properties.
    create_before_destroy = true
  }
}

# Create an EC2 instance

resource "aws_instance" "webapp" {
  ami                    = data.aws_ami.amazon_linux.id
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.webapp_security_group.id]
  key_name               = "ec2-key-pem" # Evvelceden yaratdigimiz EC2 key
  availability_zone      = var.availability_zone

  user_data = <<EOF
    #!/bin/bash
    yum install -y httpd
    echo "<h1>Deployed via Terraform</h1>" > /var/www/html/index.html
    systemctl enable httpd
    systemctl start httpd
  EOF

  tags = {
    Name      = var.instance_name
    CreatedBy = "Terraform"
    Project   = local.project
  }
}

