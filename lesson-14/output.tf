# Output config

output "instance_id" {
  description = "Instance ID"
  value       = aws_instance.webapp.id
}

output "instance_public_ip" {
  description = "Instance Public IP"
  value       = aws_instance.webapp.public_ip
}

output "instance_ami_id" {
  description = "Instance Image ID"
  value       = data.aws_ami.amazon_linux.id
}

