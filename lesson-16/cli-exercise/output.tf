# Output config

output "instance_id" {
  description = "Instance ID"
  value       = aws_instance.ec2_server.id
}

output "instance_ami_id" {
  description = "Instance Image ID"
  value       = data.aws_ami.amazon_linux.id
}
