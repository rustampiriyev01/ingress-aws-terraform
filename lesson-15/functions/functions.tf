
# Numeric functions (abs, max, min, etc.)

output "abs" {
  value = abs(-1)
}

output "max" {
  value = max(12, 54, 3)
}

# String functions (join, lower, split, etc.)

output "join" {
  value = join(":", ["aa", "bb", "cc"])
}

output "lower" {
  value = lower("HELLO")
}

# Collection functions (concat, range, lookup, etc.)

output "concat" {
  value = concat(["a", "b"], ["c", "d"])
}

output "range" {
  value = range(3)
}

# Encoding functions (base64encode, csvdecode, etc.)

output "base64encode" {
  value = base64encode("Hello World")
}

output "csvdecode" {
  value = csvdecode("a,b,c\n1,2,3")
}

# Filesystem functions (abspath, dirname, file, etc.)

output "abspath" {
  value = abspath(path.root)
}

output "file" {
  value = file("hello.txt")
}

# Date and Time functions (formatdate, timestamp, etc.)

output "formatdate" {
  value = formatdate("DD MMM YYYY hh:mm ZZZ", "2022-11-01T17:12:01Z")
}

output "timestamp" {
  value = timestamp()
}

# Hash and Crypto functions (filesha1, md5, etc.)

output "filesha1" {
  value = filesha1("hello.txt")
}

output "md5" {
  value = md5("Hello Terraform")
}

# IP Network functions (cidrhost, cidrsubnet, , etc.)

output "cidrhost" {
  value = cidrhost("10.12.112.0/20", 268)
}

output "cidrnetmask" {
  value = cidrnetmask("172.16.0.0/12")
}

# Type Conversion functions (sensitive, tomap, etc.)

output "sensitive" {
  value     = sensitive("This is a sensitive value")
  sensitive = true
}

output "tomap" {
  value = tomap({ "a" = 1, "b" = 2 })
}

