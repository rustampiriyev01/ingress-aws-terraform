# Configure AWS provider

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

# Default
provider "aws" {
  region = "us-east-1"
}

# Europe
provider "aws" {
  alias  = "europe"
  region = "eu-central-1"
}

# Asia
provider "aws" {
  alias  = "asia"
  region = "ap-south-1"
}

