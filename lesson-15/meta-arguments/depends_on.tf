data "aws_ami" "amazon_linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel*-hvm-*"]
  }
}

resource "aws_instance" "ec2_server" {
  ami                    = data.aws_ami.amazon_linux.id
  instance_type          = "t2.micro"
  vpc_security_group_ids = ["sg-075900582aa3dffa9"]
  key_name               = "ec2-key-pem"

  depends_on = [
    aws_s3_bucket.s3_bucket
  ]
}

resource "aws_s3_bucket" "s3_bucket" {
  bucket = "alpha-tf-bucket-123"
}


