# S3 buckets with different region aliases defined in provider config

resource "aws_s3_bucket" "s3_bucket_asia" {
  bucket   = "alpha-tf-bucket-asia"
  provider = aws.asia
}

resource "aws_s3_bucket" "s3_bucket_europe" {
  bucket   = "alpha-tf-bucket-europe"
  provider = aws.europe
}


