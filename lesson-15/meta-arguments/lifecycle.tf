resource "aws_security_group" "webapp_sg" {
  name   = "webapp-sg"
  vpc_id = "vpc-0fe049cb9a9224e12"

  tags = {
    Name = "webapp-sg"
  }

  lifecycle {
    # The new replacement object is created first
    # and the prior object is destroyed after the replacement is created
    create_before_destroy = true

    # Ignore changes to tags
    ignore_changes = [
      tags["Name"],
    ]
  }
}

